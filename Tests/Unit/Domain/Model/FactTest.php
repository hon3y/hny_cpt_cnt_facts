<?php
namespace HIVE\HiveCptCntFacts\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class FactTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntFacts\Domain\Model\Fact
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntFacts\Domain\Model\Fact();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubTitle()
        );
    }

    /**
     * @test
     */
    public function setSubTitleForStringSetsSubTitle()
    {
        $this->subject->setSubTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getValueReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getValue()
        );
    }

    /**
     * @test
     */
    public function setValueForFloatSetsValue()
    {
        $this->subject->setValue(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'value',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getValuePrefixReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getValuePrefix()
        );
    }

    /**
     * @test
     */
    public function setValuePrefixForStringSetsValuePrefix()
    {
        $this->subject->setValuePrefix('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'valuePrefix',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getValueSuffixReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getValueSuffix()
        );
    }

    /**
     * @test
     */
    public function setValueSuffixForStringSetsValueSuffix()
    {
        $this->subject->setValueSuffix('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'valueSuffix',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }
}
