<?php
namespace HIVE\HiveCptCntFacts\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_facts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Fact
 */
class Fact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * subTitle
     *
     * @var string
     */
    protected $subTitle = '';

    /**
     * value
     *
     * @var float
     */
    protected $value = 0.0;

    /**
     * valuePrefix
     *
     * @var string
     */
    protected $valuePrefix = '';

    /**
     * valueSuffix
     *
     * @var string
     */
    protected $valueSuffix = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subTitle
     *
     * @return string $subTitle
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Sets the subTitle
     *
     * @param string $subTitle
     * @return void
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * Returns the value
     *
     * @return float $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value
     *
     * @param float $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the valuePrefix
     *
     * @return string $valuePrefix
     */
    public function getValuePrefix()
    {
        return $this->valuePrefix;
    }

    /**
     * Sets the valuePrefix
     *
     * @param string $valuePrefix
     * @return void
     */
    public function setValuePrefix($valuePrefix)
    {
        $this->valuePrefix = $valuePrefix;
    }

    /**
     * Returns the valueSuffix
     *
     * @return string $valueSuffix
     */
    public function getValueSuffix()
    {
        return $this->valueSuffix;
    }

    /**
     * Sets the valueSuffix
     *
     * @param string $valueSuffix
     * @return void
     */
    public function setValueSuffix($valueSuffix)
    {
        $this->valueSuffix = $valueSuffix;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }
}
