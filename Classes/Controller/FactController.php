<?php
namespace HIVE\HiveCptCntFacts\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_facts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * FactController
 */
class FactController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * factRepository
     *
     * @var \HIVE\HiveCptCntFacts\Domain\Repository\FactRepository
     * @inject
     */
    protected $factRepository = null;

    /**
     * action renderFact
     *
     * @return void
     */
    public function renderFactAction()
    {

    }
}
